module.exports = function(grunt) {

  var path = require('path');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        }
      },
      myFiles: [
        '../static/public/js/*.js',
      ]
    },
    uglify: {
      default: {
        files: [{
            expand: true,
            cwd: '../static/public/js',
            src: '*.js',
            dest: '../static/public/js/min',
            ext: '.min.js'
        }]
      }
    },
    less: {
      options: {
        paths: 'static/',
        yuicompress: false,
        ieCompat: true,
        require: [
          'static/public/less/style.less'
        ]
      },
      src: {
        expand: true,
        cwd: 'static/public/less',
        src: ['**/*.less'],
        ext: '.css',
        dest: 'static/public/css',
        rename: function(dest, src) {
          return path.join(dest, src.replace(/^less/, 'css'));
        }
      }
    },
    watch: {
        files: [
          "../static/public/less/*/*",
          "../static/public/less/*",
          "../static/public/js/*",
          ],
        tasks: ["less", "jshint", "uglify"]
    }
  });

  // Load the plugins that provide the uglify, less and watch tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-preen');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['uglify', 'less', 'preen']);

};