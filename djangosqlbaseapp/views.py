from django.conf.global_settings import LANGUAGE_COOKIE_NAME
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.response import TemplateResponse
from django.utils.http import is_safe_url
from django.utils.translation import ugettext as _, check_for_language
from django.views.decorators.csrf import csrf_protect

from django.contrib.auth import authenticate, login, REDIRECT_FIELD_NAME, logout as auth_logout
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.models import get_current_site

from .forms import PasswordResetForm


def login_user(request, template_name="auth/login.html"):
    """ Logs in the user asking for 'username' and 'password'.

    """

    # Try to get next param from query string
    next_url = request.GET.get('next')

    state = None
    username = None

    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                if next_url:
                    return HttpResponseRedirect(next_url)
                else:
                    return HttpResponseRedirect(reverse('private_home'))
            else:
                state = "Your account is not active. Please, contact the system administrator."
        else:
            state = "Username and password are not correct."

    return render_to_response(template_name, {'state': state, 'username': username, 'next': next_url, },
                              context_instance=RequestContext(request))


def logout_user(request, next_page=None,
                template_name='auth/logout.html',
                redirect_field_name=REDIRECT_FIELD_NAME,
                current_app=None, extra_context=None):
    """ Logs out the user and displays 'You are logged out' message.

    """
    auth_logout(request)

    if redirect_field_name in request.REQUEST:
        next_page = request.REQUEST[redirect_field_name]
        # Security check -- don't allow redirection to a different host.
        if not is_safe_url(url=next_page, host=request.get_host()):
            next_page = request.path

    if next_page:
        # Redirect to this page until the session has been cleared.
        return HttpResponseRedirect(next_page)

    current_site = get_current_site(request)
    context = {
        'site': current_site,
        'site_name': current_site.name,
        'title': _('Logged out')
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)


@csrf_protect
def password_reset(request, is_admin_site=False,
                   template_name='email/password_reset_form.html',
                   email_template_name='email/password_reset_email.html',
                   subject_template_name='registration/password_reset_subject.txt',
                   password_reset_form=PasswordResetForm,
                   token_generator=default_token_generator,
                   post_reset_redirect=None,
                   from_email=None,
                   current_app=None,
                   extra_context=None):
    if post_reset_redirect is None:
        post_reset_redirect = reverse('django.contrib.auth.views.password_reset_done')
    if request.method == "POST":
        form = password_reset_form(request.POST)
        if form.is_valid():
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
            }
            if is_admin_site:
                opts = dict(opts, domain_override=request.get_host())
            form.save(**opts)
            return HttpResponseRedirect(post_reset_redirect)
    else:
        form = password_reset_form()
    context = {
        'form': form,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)


def set_language(request):
    """
    Redirect to a given url while setting the chosen language in the
    session or cookie. The url and the language code need to be
    specified in the request parameters.

    Since this view changes how the user will see the rest of the site, it must
    only be accessed as a POST request. If called as a GET request, it will
    redirect to the page in the request (the 'next' parameter) without changing
    any state.
    """
    path = request.REQUEST.get('next')
    n_path = path[1:].find('/') + 1
    next_url = path[n_path:]
    if not is_safe_url(url=next_url, host=request.get_host()):
        next_url = request.META.get('HTTP_REFERER')
        if not is_safe_url(url=next_url, host=request.get_host()):
            next_url = '/'
    response = HttpResponseRedirect(next_url)
    if request.method == 'GET':
        lang_code = request.GET.get('language', None)
        if lang_code and check_for_language(lang_code):
            if hasattr(request, 'session'):
                request.session['django_language'] = lang_code
            else:
                response.set_cookie(LANGUAGE_COOKIE_NAME, lang_code)
    if request.method == 'POST':
        lang_code = request.POST.get('language', None)
        if lang_code and check_for_language(lang_code):
            if hasattr(request, 'session'):
                request.session['django_language'] = lang_code
            else:
                response.set_cookie(LANGUAGE_COOKIE_NAME, lang_code)
    elif request.method == 'GET':
        lang_code = request.GET.get('language', None)
        if lang_code and check_for_language(lang_code):
            if hasattr(request, 'session'):
                request.session['django_language'] = lang_code
            else:
                response.set_cookie(LANGUAGE_COOKIE_NAME, lang_code)
    return response