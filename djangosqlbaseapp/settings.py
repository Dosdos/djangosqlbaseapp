import os
from sys import stdout



# Django settings for djangosqlbaseapp project.
from .conf import CONF

DEBUG = CONF['DEBUG']
TEMPLATE_DEBUG = DEBUG

ABSOLUTE_PATH = '%s/' % os.path.abspath(os.path.dirname(os.path.dirname(locals()['__file__']))).replace('\\', '/')

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = CONF['DATABASES']

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = CONF['ALLOWED_HOSTS']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Rome'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en'

LOCALE_PATHS = (ABSOLUTE_PATH + '/locale',)

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = ABSOLUTE_PATH + '/media/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
# STATIC_ROOT = ABSOLUTE_PATH + '/static/'

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '{0}static/'.format(ABSOLUTE_PATH),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

NO_PHOTO_URL = "https://lh5.ggpht.com/lKw_UNUbP1o5kiaw3dsHbMcmFfjsq-iOfxAAAm9b6LVjpc_QCJundBzokIOxWLUuGfI3yEjHf14r0sLSbut8I8tj"

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'tl81gb40_fr#bl7c7+yxofmark&+40v8_$6gq%0g!5g)eik9!1'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'djangosqlbaseapp.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'djangosqlbaseapp.wsgi.application'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.i18n'
)

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    ABSOLUTE_PATH + 'templates/',
)

INSTALLED_APPS = (

    # default apps
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',

    # REST Api
    'rest_framework',

    # local apps
    'public',
    'private',
    'reserved',

    # third party apps

    'bootstrap_django_tags',
    'appengine_toolkit',
    'autoslug',
    'django_appspotmail',
    'social.apps.django_app.default',

)

if CONF['SOUTH_IS_ACTIVE']:
    INSTALLED_APPS += ('south',)

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'stream': stdout
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


##########################
# Session configurations #
##########################
SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = CONF['CSRF_SECURE']
SESSION_COOKIE_NAME = 'scn'
SESSION_COOKIE_AGE = CONF['SESSION_COOKIE_AGE']    # 1 hour, in seconds
CSRF_COOKIE_NAME = 'ccn'
CSRF_COOKIE_SECURE = CONF['CSRF_SECURE']    # False along with SESSION_COOKIE_SECURE to disable csrf control when
                                            # developing in local

LOGIN_URL = '/login/'


#######################################
# Google Cloud Storage configurations #
#######################################
APPSPOT_NAME = CONF['APPSPOT_NAME']
APPSPOT_ABSOLUTE_URL = CONF['APPSPOT_ABSOLUTE_URL']
IS_CLOUD_STORAGE_ENABLED = CONF['IS_CLOUD_STORAGE_ENABLED']
if IS_CLOUD_STORAGE_ENABLED:
    APPENGINE_TOOLKIT = {
        'APP_YAML': os.path.join(ABSOLUTE_PATH, 'app.yaml'),
        'BUCKET_NAME': 'baseapp-upload',
    }
    DEFAULT_FILE_STORAGE = 'appengine_toolkit.storage.GoogleCloudStorage'
    STATICFILE_STORAGE = 'appengine_toolkit.storage.GoogleCloudStorage'

if IS_CLOUD_STORAGE_ENABLED:
    FILE_UPLOAD_HANDLERS = ('django.core.files.uploadhandler.MemoryFileUploadHandler',)
FILE_UPLOAD_MAX_MEMORY_SIZE = 20 * 1024 * 1024  # 20MB (NB: the django default is 2.5MB)


########################
# email configurations #
########################
EMAIL_SENDER_NAME = 'noreply'
EMAIL_SENDER_USERNAME = 'noreply'
EMAIL_APPSPOT_ID = CONF['APPSPOT_NAME']
EMAIL_TEAMBLE_SUPPORT = "info@domain_name.com"
EMAIL_SITENAME = "Baseapp"


######################
# Python Social Auth #
######################

LOGIN_REDIRECT_URL = '/private/'
SOCIAL_AUTH_LOGIN_ERROR_URL = '/'
MIDDLEWARE_CLASSES += (
    'social.apps.django_app.middleware.SocialAuthExceptionMiddleware',
)

# LINKEDIN
SOCIAL_AUTH_LINKEDIN_KEY = '77lvbf0y1b4zzj'
SOCIAL_AUTH_LINKEDIN_SECRET = 'FCyaGimpiycsozv5'
# Add email to requested authorizations.
SOCIAL_AUTH_LINKEDIN_SCOPE = ['r_basicprofile', 'r_emailaddress', 'r_fullprofile']
# Add the fields so they will be requested from linkedin.
SOCIAL_AUTH_LINKEDIN_FIELD_SELECTORS = ['email-address', 'positions', 'headline', 'summary', 'picture-url',
                                        'site-standard-profile-request', 'public-profile-url', 'location', 'interests',
                                        'skills', 'languages', ]
# Arrange to add the fields to UserSocialAuth.extra_data
SOCIAL_AUTH_LINKEDIN_EXTRA_DATA = [('id', 'id'),
                                   ('firstName', 'first_name'),
                                   ('lastName', 'last_name'),
                                   ('emailAddress', 'email_address'),
                                   ('headline', 'headline'),
                                   ('industry', 'industry'),
                                   ('positions', 'positions'),
                                   ('summary', 'summary'),
                                   ('headline', 'headline'),
                                   ('pictureUrl', 'picture_url'),
                                   ('siteStandardProfileRequest', 'site_standard_profile_request'),
                                   ('publicProfileUrl', 'public_profile_url'),
                                   ('location', 'location'),
                                   ('interests', 'interests'),
                                   ('skills', 'skills'),
                                   ('languages', 'languages'), ]

# FACEBOOK
SOCIAL_AUTH_FACEBOOK_KEY = '292614927603152'
SOCIAL_AUTH_FACEBOOK_SECRET = 'ab8e01fa9604b3795b5faed6fa35d6b5'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.linkedin.LinkedinOAuth',
    'social.backends.twitter.TwitterOAuth',
    'django.contrib.auth.backends.ModelBackend',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
)

# PYTHON-SOCIAL-AUTH PIPELINE
SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    # 'social.pipeline.social_auth.associate_by_email',     # <--- avoid multiple mail account (NB: is not secure)
    'social.pipeline.user.create_user',
    # 'path.to.function.for.user.customization              # <--- eg: configure a UserProfile
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details'
)

# TWITTER
SOCIAL_AUTH_TWITTER_KEY = 'Zt8GOMvNF2JOXVdUIhsKvMZRJ'
SOCIAL_AUTH_TWITTER_SECRET = '8NzwRLGTtbkKguwSP1RAGwBhzw7rl41AZUzcboT9geUfw7XOtL'


#########################
# Django REST Framework #
#########################
REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions, or allow read-only access for unauthenticated users.
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',),
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAuthenticated',),
    'PAGINATE_BY': 10,
}
