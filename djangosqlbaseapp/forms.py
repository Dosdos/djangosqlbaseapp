from django import forms
from django.utils.http import int_to_base36
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import UNUSABLE_PASSWORD
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.models import get_current_site

from .settings import EMAIL_SITENAME
from django_appspotmail.email_messenger import EmailMessenger
from utils.debug_manager import custom_logger


class PasswordResetForm(forms.Form):
    """ Refactoring of django/contrib/auth/forms.py
    """
    error_messages = {
        'unknown': _("That email address doesn't have an associated "
                     "user account. Are you sure you've registered?"),
        'unusable': _("The user account associated with this email "
                      "address cannot reset the password."),
    }
    email = forms.EmailField(label=_("Email"), max_length=254)

    def clean_email(self):
        """ Validates that an active user exists with the given email address.
        """
        usermodel = get_user_model()
        email = self.cleaned_data["email"]
        self.users_cache = usermodel._default_manager.filter(email__iexact=email)
        if not len(self.users_cache):
            raise forms.ValidationError(self.error_messages['unknown'])
        if not any(user.is_active for user in self.users_cache):
            # none of the filtered users are active
            raise forms.ValidationError(self.error_messages['unknown'])
        if any((user.password == UNUSABLE_PASSWORD)
               for user in self.users_cache):
            raise forms.ValidationError(self.error_messages['unusable'])
        return email

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None):
        """ Generates a one-use only link for resetting password and sends to the user.
        """
        for user in self.users_cache:
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override

            email_data = {
                'email': user.email,
                'domain': domain,
                'site_name': site_name,
                'uid': int_to_base36(user.pk),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': use_https and 'https' or 'http',
            }

            # Create a new EmailMessenger object in order to send custom email with appspotmail
            messenger = EmailMessenger()
            send_is_successfully = messenger.send_email(user.email,
                                                        user.username,
                                                        "Password reset - %s" % EMAIL_SITENAME,
                                                        "email/password_reset_email.html",
                                                        "email/password_reset_email_html.html",
                                                        **email_data)

            if not send_is_successfully:
                custom_logger("Unexpected exception while sending the email, check logs.", email_data, 'warning')
