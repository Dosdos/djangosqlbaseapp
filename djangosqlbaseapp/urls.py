from django.conf.urls import patterns, i18n, include, url
from django.conf import settings
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
    # admin
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # i18n
    url(r'^i18n/', 'djangosqlbaseapp.views.set_language', {}, 'set_language'),
    # Authentication
    url(r'^login/$', 'djangosqlbaseapp.views.login_user', {'template_name': 'auth/login.html'}, 'login'),
    url(r'^logout/$', 'djangosqlbaseapp.views.logout_user', {'template_name': 'auth/logout.html'}, 'logout'),
    url(r'^password_change/$', 'django.contrib.auth.views.password_change', {'template_name': 'auth/password_change.html'}, 'password_change'),
    url(r'^password_change_done/$', 'django.contrib.auth.views.password_change_done', {'template_name': 'auth/password_change_done.html'}, 'password_change_done'),
    url(r'^password/reset/$', 'djangosqlbaseapp.views.password_reset', {'post_reset_redirect': '/password/reset/done/', 'template_name': 'auth/password_reset_form.html'}, 'password_reset'),
    url(r'^password/reset/done/$', 'django.contrib.auth.views.password_reset_done', {'template_name': 'auth/password_reset_done.html'}, ),
    url(r'^password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', {'post_reset_redirect': '/password/done/', 'template_name': 'auth/password_reset_confirm.html'}, ),
    url(r'^password/done/$', 'django.contrib.auth.views.password_reset_complete', {'template_name': 'auth/password_reset_complete.html'}, ),

    # third party apps
    url('', include('social.apps.django_app.urls', namespace='social')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        # Media
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, }),
        # Statics
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT, }),
    )

urlpatterns += i18n.i18n_patterns('',
    # private
    url(r'^private/', include('private.urls')),
    # reserved
    url(r'^reserved/', include('reserved.urls')),
    # public
    url(r'^', include('public.urls')),
)
