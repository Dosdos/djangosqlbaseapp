#############################
#   BRANCH CONFIGURATIONS   #
#############################

CLOUD_SQL_INSTANCE_ID = 'djangosqlbaseapp:db'
CLOUD_SQL_HOST_ADDRESS = '173.194.246.236'
CLOUD_SQL_ADMIN_NAME = 'root'
CLOUD_SQL_ADMIN_PW = 'stlm1pOLLDSRzbvnyHaq'


# Configurations for 'LOCAL'
LOCAL_CONF = {
    # Appengine configurations
    'APPSPOT_NAME': '',
    'APPSPOT_ABSOLUTE_URL': 'http://127.0.0.1:8000',

    'DATABASES': {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'djangosqlbaseapp_local_db',
            'USER': 'root',
            'PASSWORD': 'root',
            'HOST': 'localhost',  # Or an IP Address that your DB is hosted on
            'PORT': '3306',
        }
    },

    'SESSION_COOKIE_AGE': 60 * 60 * 24 * 30 * 2,  # Cookie age: about 2 months, in seconds
    'CSRF_SECURE': False,  # False disables csrf control when developing in local
    'DEBUG': True,
    'ALLOWED_HOSTS': ['127.0.0.1', 'localhost'],
    'SOUTH_IS_ACTIVE': True,
    'IS_CLOUD_STORAGE_ENABLED': False,
}


# Configurations for 'DEVELOPMENT'
DEV_SYNC_CONF = {
    # Appengine configurations
    'APPSPOT_NAME': '2-dot-djangosqlbaseapp',
    'APPSPOT_ABSOLUTE_URL': 'https://2-dot-djangosqlbaseapp.appspot.com',

    # The actual Cloud SQL ip and user auth coordinates: use when in need to syncdb, shell
    'DATABASES': {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'dev_db',
            'USER': CLOUD_SQL_ADMIN_NAME,
            'PASSWORD': CLOUD_SQL_ADMIN_PW,
            'HOST': CLOUD_SQL_HOST_ADDRESS,  # Or an IP Address that your DB is hosted on
            'PORT': '3306',
        }
    },

    'SESSION_COOKIE_AGE': 60 * 60 * 24,  # Cookie age: a day in seconds
    'CSRF_SECURE': True,
    'DEBUG': True,
    'ALLOWED_HOSTS': ['2-dot-djangosqlbaseapp.appspot.com'],
    'SOUTH_IS_ACTIVE': True,
    'IS_CLOUD_STORAGE_ENABLED': False,
}


DEV_CONF = {
    # Appengine configurations
    'APPSPOT_NAME': '2-dot-djangosqlbaseapp',
    'APPSPOT_ABSOLUTE_URL': 'https://2-dot-djangosqlbaseapp.appspot.com',

    'DATABASES': {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'HOST': '/cloudsql/'+CLOUD_SQL_INSTANCE_ID,
            'NAME': 'dev_db',
            'USER': CLOUD_SQL_ADMIN_NAME,
        }
    },

    'SESSION_COOKIE_AGE': 60 * 60 * 24,  # Cookie age: a day in seconds
    'CSRF_SECURE': True,
    'DEBUG': True,
    'ALLOWED_HOSTS': ['2-dot-djangosqlbaseapp.appspot.com'],
    'SOUTH_IS_ACTIVE': False,
    'IS_CLOUD_STORAGE_ENABLED': True,
}




# Configurations for 'PRODUCTION'
PROD_SYNC_CONF = {
    # Appengine configurations
    'APPSPOT_NAME': 'djangosqlbaseapp',
    'APPSPOT_ABSOLUTE_URL': 'https://djangosqlbaseapp.appspot.com',

    # The actual Cloud SQL ip and user auth coordinates: use when in need to syncdb, shell
    'DATABASES': {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'db',
            'USER': CLOUD_SQL_ADMIN_NAME,
            'PASSWORD': CLOUD_SQL_ADMIN_PW,
            'HOST': CLOUD_SQL_HOST_ADDRESS,  # Or an IP Address that your DB is hosted on
            'PORT': '3306',
        }
    },

    'SESSION_COOKIE_AGE': 60 * 60 * 24,  # Cookie age: a day in seconds
    'CSRF_SECURE': True,
    'DEBUG': False,
    'ALLOWED_HOSTS': ['djangosqlbaseapp.appspot.com'],
    'SOUTH_IS_ACTIVE': True,
    'IS_CLOUD_STORAGE_ENABLED': False,
}

PROD_CONF = {
    # Appengine configurations
    'APPSPOT_NAME': 'djangosqlbaseapp',
    'APPSPOT_ABSOLUTE_URL': 'https://djangosqlbaseapp.appspot.com',

    'DATABASES': {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'HOST': '/cloudsql/'+CLOUD_SQL_INSTANCE_ID,
            'NAME': 'db',
            'USER': CLOUD_SQL_ADMIN_NAME,
        }
    },

    'SESSION_COOKIE_AGE': 60 * 60,  # Cookie age: an hour in seconds
    'CSRF_SECURE': True,
    'DEBUG': False,
    'ALLOWED_HOSTS': ['djangosqlbaseapp.appspot.com'],
    'SOUTH_IS_ACTIVE': False,
    'IS_CLOUD_STORAGE_ENABLED': True,
}

# Configurations dicts mapping
CONF_DICT = {
    'local': LOCAL_CONF,
    'dev_sync': DEV_SYNC_CONF,
    'dev': DEV_CONF,
    'prod_sync': PROD_SYNC_CONF,
    'prod': PROD_CONF,
}

# Edit dict key to select the current branch
CONF = CONF_DICT['dev_sync']
