from rest_framework import serializers


class InviteNetworkSerializer(serializers.Serializer):
    subject = serializers.CharField(max_length=100)
    emails = serializers.WritableField()