import json
from smtplib import SMTPException

from django.core.mail import send_mail
from django.http.response import HttpResponse
from django.template import loader
from django.template.context import Context
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from bitstuff.settings import DEFAULT_FROM_EMAIL
from network.serializers import InviteNetworkSerializer


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def create_network_email(request):
    serializer = InviteNetworkSerializer(data=request.DATA)
    if not serializer.is_valid():
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "serializer not valid"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    #
    object = serializer.object
    subject = object['subject']
    emails = object['emails']
    #
    t = loader.get_template('network/create_network_email.html')
    c = Context({
        'user': request.user,
    })
    try:
        send_mail(subject, t.render(c), DEFAULT_FROM_EMAIL, emails, fail_silently=False)
    except SMTPException as e:
        # failure
        response_data = {'errorCode': 2, 'errorMessage': "send mail exception "}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    # success
    response_data = {'errorCode': 0, 'errorMessage': ""}
    return HttpResponse(json.dumps(response_data), content_type="application/json")