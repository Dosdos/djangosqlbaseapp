from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render_to_response
from django.template import RequestContext


def has_reserved_access(user):
    return user.groups.filter(name='reserved')


@login_required(login_url='/login/')
@user_passes_test(has_reserved_access)
def home(request, template_name="public/home.html"):
    """ Simply render the public home page.

    """

    return render_to_response(template_name, {
    'session': request.session.keys(),
    }, context_instance=RequestContext(request))