from django.conf.urls import patterns, url, include
from private.views import ExampleModelsList, ExampleModelDetail


urlpatterns = patterns('private.views',
        # Auth: Signup
        (r'^signup[/]?$', 'signup', {'template_name': 'auth/signup.html'}, 'signup'),

        # Local urls
        (r'^$', 'home', {'template_name': 'private/home.html'}, 'private_home'),
        (r'^example_models[/]?$', 'example_models', {'template_name': 'private/example_model/example_models.html'}, 'example_models'),
        (r'^example_model_create[/]?$', 'example_model_create', {'template_name': 'private/example_model/example_model_create.html'}, 'example_model_create'),
        (r'^example_model/(?P<example_model_slug>[-\w]+)[/]?$', 'example_model', {'template_name': 'private/example_model/example_model.html'}, 'example_model'),
        (r'^example_model/(?P<example_model_slug>[-\w]+)/edit[/]?$', 'example_model_edit', {'template_name': 'private/example_model/example_model_edit.html'}, 'example_model_edit'),
        (r'^example_model/(?P<example_model_slug>[-\w]+)/delete[/]?$', 'example_model_delete', {}, 'example_model_delete'),

)


#########################################
# Django REST framework                 #
# http://www.django-rest-framework.org/ #
#########################################
urlpatterns += patterns('',
    # Example Model
    url(r'^api/example_models/$', ExampleModelsList.as_view()),
    url(r'^api/example_model_create/$', 'private.views.post_example_model'),
    url(r'^api/example_model/(?P<example_model_slug>[-\w]+)/$', ExampleModelDetail.as_view()),


    # Api Auth
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

)