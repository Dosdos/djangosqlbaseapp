from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import pre_save, post_delete
from autoslug import AutoSlugField
from private.validators import isTrue

from utils.signals import remove_files_on_model_update, remove_files_on_model_deletion


class UserProfile(models.Model):
    # This field is required.
    user = models.OneToOneField(User)

    # Other fields here
    accepted_eula = models.BooleanField(default=False)
    favorite_animal = models.CharField(max_length=20, default="Groundhog.")
    has_accepted_terms_and_conditions = models.BooleanField(validators=[isTrue], default=False, )

    def __unicode__(self):
        return u'%s' % self.user.username


class ExampleModel(models.Model):
    name = models.CharField(max_length=30, unique=True, )
    slug = AutoSlugField(populate_from='name', unique=True, )
    logo = models.ImageField(upload_to='private/example_model/logo', null=True, blank=False, )
    description = models.TextField()
    is_public = models.BooleanField(default=False, )

    def __unicode__(self):
        return u'%s' % self.name


# ##########
# Signals #
###########

# Event
pre_save.connect(remove_files_on_model_update, sender=ExampleModel)
post_delete.connect(remove_files_on_model_deletion, sender=ExampleModel)