from django.contrib import admin
from .models import UserProfile, ExampleModel


class ExampleModelAdmin(admin.ModelAdmin):
    list_filter = ('is_public', )
    list_display = ('name',
                    'slug',
                    'logo',
                    'description',
                    'is_public',
                    )
    list_display_links = ('name', 'slug', )
    save_on_top = True
    ordering = ('name',)
    list_per_page = 20



admin.site.register(UserProfile)
admin.site.register(ExampleModel, ExampleModelAdmin)