from django.core.exceptions import ValidationError


def isTrue(value):
    if not value:
        raise ValidationError(u'You need to check this field to proceed')