from rest_framework import serializers
from .models import ExampleModel


class ExampleModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExampleModel
        fields = ('name',
                  'slug',
                  'logo',
                  'description',
                  'is_public',
        )


class ExampleModelCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExampleModel
        fields = ('name',
                  'description',
                  'is_public',
        )