from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect

from .forms import UserProfileCreationForm, ExampleModelForm, CustomUserCreationForm
from .models import ExampleModel
from utils.debug_manager import custom_logger


def signup(request, template_name="auth/signup.html"):
    """ Render the signup page.

    """
    if request.method == 'POST':
        user_form = CustomUserCreationForm(request.POST)
        user_profile_form = UserProfileCreationForm(request.POST)

        if user_form.is_valid() and user_profile_form.is_valid():
            user = user_form.save()
            user_profile = user_profile_form.save(commit=False)
            user_profile.user = user
            user_profile.save()

            return HttpResponseRedirect( reverse('private_home') )
    else:
        user_form = CustomUserCreationForm()
        user_profile_form = UserProfileCreationForm()


    return render_to_response(template_name, {
        'user_form': user_form,
        'user_profile_form': user_profile_form,
        }, context_instance=RequestContext(request))



@login_required(login_url='/login/')
def home(request, template_name="private/home.html"):
    """ Simply render the public home page.

    """

    return render_to_response(template_name, {
        'session': request.session.keys(),
        }, context_instance=RequestContext(request))



# ===========================================================================
# EXAMPLE MODEL
# ===========================================================================


@login_required(login_url='/login/')
def example_models(request, template_name="private/example_model/example_models.html"):
    return render_to_response(template_name, {
    'session': request.session.keys(),
    'example_models': ExampleModel.objects.all(),
    }, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def example_model(request, example_model_slug, template_name="private/example_model/example_model.html"):
    try:
        example_model = ExampleModel.objects.get(slug=example_model_slug)
    except ExampleModel.DoesNotExist:
        custom_logger("a ExampleModel with this slug doesn't exist", {'example_model_slug': example_model_slug, })
        return HttpResponseRedirect(reverse('example_models'))

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'example_model': example_model,
    }, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def example_model_create(request, template_name="private/example_model/example_model_create.html"):
    if request.method == 'POST':
        example_model_form = ExampleModelForm(request.POST, request.FILES)
        if example_model_form.is_valid():
            example_model = example_model_form.save()
            return HttpResponseRedirect(reverse('example_model', args=(example_model.slug,)))
    else:
        example_model_form = ExampleModelForm()

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'example_model_form': example_model_form,
    }, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def example_model_edit(request, example_model_slug, template_name="private/example_model/example_model_edit.html"):
    try:
        example_model = ExampleModel.objects.get(slug=example_model_slug)
    except ExampleModel.DoesNotExist:
        custom_logger("a ExampleModel with this slug doesn't exist", {'example_model_slug': example_model_slug, })
        return HttpResponseRedirect(reverse('example_models'))

    if request.method == 'POST':
        example_model_form = ExampleModelForm(request.POST, request.FILES, instance=example_model)
        if example_model_form.is_valid():
            example_model_form.save()
            return HttpResponseRedirect(reverse('example_model', args=(example_model_slug,)))
    else:
        example_model_form = ExampleModelForm(instance=example_model)

    return render_to_response(template_name, {
        'session': request.session.keys(),
        'example_model': example_model,
        'example_model_form': example_model_form,
    }, context_instance=RequestContext(request))


@login_required(login_url='/login/')
def example_model_delete(request, example_model_slug):
    try:
        example_model = ExampleModel.objects.get(slug=example_model_slug)
        example_model.delete()
    except ExampleModel.DoesNotExist:
        custom_logger("a ExampleModel with this slug doesn't exist", {'example_model_slug': example_model_slug, })

    return HttpResponseRedirect(reverse('example_models'))


#########################################
# Django REST framework views           #
# http://www.django-rest-framework.org/ #
#########################################
from .serializer import ExampleModelSerializer, ExampleModelCreateSerializer
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response


class ExampleModelsList(generics.ListAPIView):
    serializer_class = ExampleModelSerializer

    def get_queryset(self):
        return ExampleModel.objects.all()


class ExampleModelDetail(generics.ListAPIView):
    serializer_class = ExampleModelSerializer

    def get_queryset(self):
        example_model_slug = self.kwargs['example_model_slug']
        return ExampleModel.objects.filter(slug=example_model_slug)


@api_view(['POST'])
def post_example_model(request):
    from rest_framework import status

    if request.method == 'POST':
        serializer = ExampleModelCreateSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
