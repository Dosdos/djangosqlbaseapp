from django import forms
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import UserProfile, ExampleModel



class CustomUserCreationForm(UserCreationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'type':"text", 'class':"form-control", }))
    password1 = forms.CharField(widget=forms.TextInput(attrs={'type':"password", 'class':"form-control", }))
    password2 = forms.CharField(widget=forms.TextInput(attrs={'type':"password", 'class':"form-control", }))
    email = forms.EmailField(widget=forms.TextInput(attrs={'type':"text", 'class':"form-control", }))

    class Meta:
        model = User
        fields = (
            'username',
            'password1',
            'password2',
            'email',
        )

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if email and User.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError(u'A Hacker is using that email address already.')
        return email

    def save(self, commit=True):
        user = super(CustomUserCreationForm, self).save(commit=False)
        user.username = self.cleaned_data["username"]
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user


class UserProfileCreationForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = (
            'accepted_eula',
            'favorite_animal',
            'has_accepted_terms_and_conditions',
        )

        widgets = {
            'accepted_eula': forms.CheckboxInput(attrs={'type': "checkbox", 'class': "uniform", 'placeholder': "", }),
            'favorite_animal': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Name", }),
            'has_accepted_terms_and_conditions': forms.CheckboxInput(attrs={'type': "checkbox", 'class': "uniform", 'placeholder': "", }),
        }

class ExampleModelForm(forms.ModelForm):
    class Meta:
        model = ExampleModel

        fields = (
            'name',
            'logo',
            'description',
            'is_public',
        )

        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Name", }),
            'logo': forms.FileInput(attrs={'class': "form-control", }),
            'description': forms.Textarea(attrs={'class': "form-control", 'placeholder': "Description", }),
            'is_public': forms.CheckboxInput(attrs={'type':"checkbox", 'class':"uniform", }) ,
        }

    def clean_logo(self):
        logo = self.cleaned_data.get('logo', False)
        if logo:
            if len(logo) > settings.FILE_UPLOAD_MAX_MEMORY_SIZE:
                raise forms.ValidationError(u"Image file too large (maximum admitted is %dMB)" % (settings.FILE_UPLOAD_MAX_MEMORY_SIZE / (1024 * 1024)))
            return logo
        else:
            raise forms.ValidationError(u"Couldn't read uploaded image")

