"""
Python Appengine documentation here:
https://developers.google.com/appengine/docs/python/mail/

"""

from django.template import loader
from django.template.context import Context
from django.conf import settings
from utils.debug_manager import custom_logger

# Try to import appengine mail api (try-except allows local development)
try:
    from google.appengine.api import mail
except ImportError:
    pass


EMAIL_SENDER_NAME = getattr(settings, 'EMAIL_SENDER_NAME', None)
EMAIL_SENDER_USERNAME = getattr(settings, 'EMAIL_SENDER_USERNAME', None)
EMAIL_APPSPOT_ID = getattr(settings, 'EMAIL_APPSPOT_ID', None)


class EmailMessenger(object):
    """
    Python Appengine documentation here:
    https://developers.google.com/appengine/docs/python/mail/

    Message Fields:
    https://developers.google.com/appengine/docs/python/mail/emailmessagefields
    """

    def __init__(self, sender_name=None, sender_username=None):

        # Compose the sender email address from name and username
        self.sender_email_address = "%s <%s@%s.appspotmail.com>" % (
                                    sender_name if sender_name else EMAIL_SENDER_NAME,
                                    sender_username  if sender_username else EMAIL_SENDER_USERNAME,
                                    EMAIL_APPSPOT_ID,
                                    )

    def send_email(self,
                   recipient_email_address,
                   recipient_name,
                   email_subject,
                   email_message_template_name,
                   email_html_template_name,
                   **email_template_context):
        """ Send an email.

        Don't use unicode strings in message.body, because the mail server can't render them.

        Args:
            recipient_email_address: The email address of the recipient
            recipient_name: The name of the recipient
            email_subject: The email subject
            email_message_template_name: Absolute path to plain-text email template
            email_html_template_name: Absolute path to html email template
            email_template_context: Data to be rendered in the mail body

        Returns:
            True is the email has been correctly initialized and sent, False otherwise.
        """

        # Email subject *must not* contain newlines
        email_subject = ''.join(email_subject.splitlines())

        # Create a new email message with appengine api
        message = mail.EmailMessage(sender=self.sender_email_address, subject=email_subject)

        # Set the recipient of the email
        message.to = "%s <%s>" % ( recipient_name, recipient_email_address)

        # Set the body of the email
        text_template = loader.get_template(email_message_template_name)
        message.body = text_template.render(Context(email_template_context))

        # Set the html template if provided
        html_template = loader.get_template(email_html_template_name)
        message.html = html_template.render(Context(email_template_context))

        try:
            # and finally try to send the email
            message.check_initialized()
            message.send()
            return True
        except Exception as e:
            custom_logger("Something goes wrong ...", {'e': e, 'e.args': e.args, 'type(e)': type(e), }, 'warning')
            return False
