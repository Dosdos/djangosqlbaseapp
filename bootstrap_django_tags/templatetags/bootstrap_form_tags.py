from django import template


register = template.Library()


@register.inclusion_tag("bootstrap_django_tags/horizontal_form_input.html")
def horizontal_form_input(form_field, label=None, required=True):
    """ Returns the horizontal form input in bootstrap format.
    
    insert the following in the template: 
    {% load bootstrap_form_tags %}{% form_input form_field_input field_label required %}
    """

    return {
        'form_field': form_field,
        'label': label if label else form_field.html_name,
        'required': required,
    }


@register.inclusion_tag("bootstrap_django_tags/horizontal_form_submit.html")
def horizontal_form_submit(submit_button_label='Submit', button_class="default"):
    """ Returns the horizontal form input in bootstrap format.

    insert the following in the template:
    {% load bootstrap_form_tags %}{% form_submit %}
    """

    button_classes = ["default", "primary", "success", "info", "warning", "danger", "link"]

    return {
        'submit_button_label': submit_button_label,
        'button_class': button_class if button_class in button_classes else "default",
    }

@register.inclusion_tag("bootstrap_django_tags/inline_form_input.html")
def inline_form_input(form_field, label=None):
    """ Returns the horizontal form input in bootstrap format.

    insert the following in the template:
    {% load bootstrap_form_tags %}{% form_input form_field_input field_label %}
    """

    return {
        'form_field': form_field,
        'label': label if label else form_field.html_name,
    }

@register.inclusion_tag("bootstrap_django_tags/inline_form_submit.html")
def inline_form_submit(submit_button_label='Submit', button_class="default"):
    """ Returns the horizontal form input in bootstrap format.

    insert the following in the template:
    {% load bootstrap_form_tags %}{% form_submit %}
    """

    button_classes = ["default", "primary", "success", "info", "warning", "danger", "link"]

    return {
        'submit_button_label': submit_button_label,
        'button_class': button_class if button_class in button_classes else "default",
    }

@register.inclusion_tag("bootstrap_django_tags/basic_form_input.html")
def basic_form_input(form_field, label=None, required=True):
    """ Returns the horizontal form input in bootstrap format.

    insert the following in the template:
    {% load bootstrap_form_tags %}{% form_input form_field_input field_label required %}
    """

    return {
        'form_field': form_field,
        'label': label if label else form_field.html_name,
        'required': required,
    }

@register.inclusion_tag("bootstrap_django_tags/basic_form_submit.html")
def basic_form_submit(submit_button_label='Submit', button_class="default"):
    """ Returns the horizontal form input in bootstrap format.

    insert the following in the template:
    {% load bootstrap_form_tags %}{% form_submit %}
    """

    button_classes = ["default", "primary", "success", "info", "warning", "danger", "link"]

    return {
        'submit_button_label': submit_button_label,
        'button_class': button_class if button_class in button_classes else "default",
    }