from django.shortcuts import render_to_response
from django.template import RequestContext


def home(request, template_name="public/home.html"):
    """ Simply render the public home page.

    """

    return render_to_response(template_name, {
    'session': request.session.keys(),
    }, context_instance=RequestContext(request))