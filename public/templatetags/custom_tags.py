from django import template
from django.conf import settings


register = template.Library()


def url_https(image, size=None, crop=False):
    """
    Django template tag to force https URL references.

    Insert the following script in template to create the complete url:
    {% load custom_tags %}{% url_https your_url %}

    Args:
        image: the image to force https
    """
    if image:
        return_url = image.url
    else:
        return_url = settings.NO_PHOTO_URL

    if settings.IS_CLOUD_STORAGE_ENABLED:
        return_url = return_url.replace("http://", "https://", 1)
        if size:
            return_url += "=s%s" % (str(size))
            if crop:
                return_url += "-c"

    return return_url


def fb_app_id():
    """
    Returns the string contained in the setting SOCIAL_AUTH_FACEBOOK_KEY.

    insert the following in the template:
    {% load custom_tags %}{% fb_app_id %}
    """
    return settings.SOCIAL_AUTH_FACEBOOK_KEY


def domain_url():
    """
    Returns the string contained in the setting APPSPOT_ABSOLUTE_URL.

    insert the following in the template:
    {% load custom_tags %}{% domain_url %}
    """
    return settings.APPSPOT_ABSOLUTE_URL


url_https = register.simple_tag(url_https)
fb_app_id = register.simple_tag(fb_app_id)
domain_url = register.simple_tag(domain_url)

