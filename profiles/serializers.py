from django.contrib.auth.models import User
from rest_framework import serializers

from profiles.models import UserProfile


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email')


class UserRegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=100)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password')


class UserUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class UserProfileSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    username = serializers.CharField(source='user.username')
    email = serializers.CharField(source='user.email', required=False)

    class Meta:
        model = UserProfile
        fields = ('id', 'first_name', 'last_name', 'username', 'email', 'ghost', 'hunter',
                  'born', 'city', 'sex', 'facebook_id', 'google_plus_id')


class ResponseStatusSerializer(serializers.Serializer):
    errorCode = serializers.IntegerField()
    errorMessage = serializers.CharField()