# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('born', models.CharField(max_length=20, null=True, blank=True)),
                ('city', models.CharField(max_length=200, null=True, blank=True)),
                ('sex', models.CharField(max_length=2, null=True, blank=True)),
                ('facebook_id', models.CharField(max_length=100, null=True, blank=True)),
                ('google_plus_id', models.CharField(max_length=100, null=True, blank=True)),
                ('user', models.OneToOneField(related_name=b'profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
