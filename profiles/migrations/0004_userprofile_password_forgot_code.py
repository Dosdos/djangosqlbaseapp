# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import profiles.models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_userprofile_hunter'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='password_forgot_code',
            field=models.CharField(default=profiles.models.generate_random_string, max_length=4),
            preserve_default=True,
        ),
    ]
