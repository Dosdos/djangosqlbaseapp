import logging


def custom_logger(message, data={}, severity='error'):
    """Print logs on appengine console.
    
    The Python logging module allows a developer to log 5 levels of severity:
    Debug, Info, Warning, Error and Critical.
    See docs here: https://developers.google.com/appengine/articles/logging
    
    Args:
        message: the string message of error.
        data: a dict containing data to print on log.
        severity: the level of severity. One of the following: 'debug', 'info',
            'warning', 'error' or 'critical' (default: 'error')
    Returns:
        void
    """
    
    # Create the final message
    final_msg = message
    if len(data.keys()) > 0:
        final_msg +=' --- data: ' + str(data)
    
    #### DEBUG ####
    if severity == 'debug':
        logging.getLogger().setLevel(logging.DEBUG)
        logging.debug(final_msg)
    
    #### INFO ####
    elif severity == 'info':
        logging.info(final_msg)
    
    #### WARNING ####
    elif severity == 'warning':
        logging.warning(final_msg)
    
    #### ERROR ####
    elif severity == 'error':
        logging.error(final_msg)
    
    #### CRITICAL ####
    elif severity == 'critical':
        logging.critical(final_msg)
