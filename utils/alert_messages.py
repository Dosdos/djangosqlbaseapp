# coding: utf-8


#===============================================================================
# MESSAGES MAPPING
#===============================================================================


# set of available error messages
ERROR = {
    r'demo_error_message': r"This a demo error message.",
}

# set of available success messages
SUCCESS = {
    r'demo_success_message': r"This a demo success message.",
}

# set of available info messages
INFO = {
    r'demo_info_message': r"This a demo info message.",
}

# set of available warning messages
WARNING = {
    r'demo_warning_message': r"This a demo warning message.",

}
