import json

from django.contrib.auth.models import User
from django.http.response import HttpResponse
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.renderers import JSONRenderer

from peripherals.models import Peripheral, LostAndFound, SecurityArea
from peripherals.serializers import PeripheralSerializer, PermissionsSerializer, LostAndFoundSerializer, \
    SecurityAreaSerializer, LostAndFoundCreateSerializer
from profiles.models import UserProfile
from userdevices.models import UserDevice


@permission_classes((IsAuthenticated,))
class PeripheralsView(generics.ListAPIView):
    model = Peripheral
    serializer_class = PeripheralSerializer

    def get_queryset(self):
        profile = UserProfile.objects.get(user=self.request.user)
        return Peripheral.objects.filter(owners=profile)


class PermissionsView(generics.RetrieveAPIView):
    model = UserProfile
    serializer_class = PermissionsSerializer

    def get_object(self, queryset=None):
        profile = self.request.user.profile
        profile_losts = LostAndFound.objects.filter(enabled=True, lost_owner=profile)
        profile.mylost = profile_losts
        return profile


@api_view(['POST'])
@permission_classes((AllowAny,))
def activate_peripheral_temporary_user(request):
    # get code
    if not 'code' in request.DATA:
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "code not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    code = request.DATA['code']
    # get device_unique_id
    if not 'device_unique_id' in request.DATA:
        # failure
        response_data = {'errorCode': 2, 'errorMessage': "device_unique_id not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    device_unique_id = request.DATA['device_unique_id']
    # get userdevice_id
    if not 'userdevice_id' in request.DATA:
        # failure
        response_data = {'errorCode': 3, 'errorMessage': "code not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    code = request.DATA['code']
    # search a peripheral with a specific activation_code
    peripherals = Peripheral.objects.filter(activation_code=code)
    count = peripherals.count()
    if count == 1:
        peripheral = peripherals.all()[0]

        # get UserDevice with device_unique_id
        try:
            userdevice = UserDevice.objects.get(device_unique_id=device_unique_id)
        except:
            # failure
            response_data = {'errorCode': 4, 'errorMessage': 'cannot find userdevice with UUID: %s' % device_unique_id}
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        # activate association
        # create temporary user associated with this userdevice
        username = userdevice.device_unique_id
        email = ""
        password = ""
        user = User.objects.create_user(username, email, password)
        if not user:
            # failure
            response_data = {'errorCode': 35, 'errorMessage': 'cannot save user'}
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        # create profile for user
        profile = UserProfile()
        profile.user = user
        profile.save()

        # create relation between temporary user and peripheral
        peripheral.owners.add(profile)

        # success
        response_data = {'errorCode': 0, 'errorMessage': ''}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    elif count > 0:
        response_data = {'errorCode': 6, 'errorMessage': 'multiple peripherals found with the provided code'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        response_data = {'errorCode': 7, 'errorMessage': 'no peripheral found with provided code'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")


@api_view(['POST'])
def activate_peripheral(request):
    # get code
    if not 'code' in request.DATA:
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "POST code not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    code = request.DATA['code']
    # search a peripheral with a specific activation_code
    peripherals = Peripheral.objects.filter(activation_code=code)
    count = peripherals.count()
    if count == 1:
        peripheral = peripherals.all()[0]
        # activate association
        profile = request.user.profile
        peripheral.owners.add(profile)
        peripheral.save()
        # serialize peripheral
        serializer = PeripheralSerializer(instance=peripheral)
        response_data = JSONRenderer().render(serializer.data)
        return HttpResponse(response_data, content_type="application/json")
    elif count > 0:
        response_data = {'errorCode': 1, 'errorMessage': 'multiple peripherals found with the provided code'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        response_data = {'errorCode': 2, 'errorMessage': 'no peripheral found with provided code'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")


@api_view(['POST'])
def connect_peripheral(request):
    # get code
    if not 'major' in request.DATA:
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "POST major not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    major = request.DATA['major']
    if not 'minor' in request.DATA:
        # failure
        response_data = {'errorCode': 2, 'errorMessage': "POST minor not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    minor = request.DATA['minor']
    # search a peripheral with a specific major and minor
    peripherals = Peripheral.objects.filter(major=major, minor=minor)
    count = peripherals.count()
    if count == 1:
        peripheral = peripherals.all()[0]
        # activate association
        profile = request.user.profile
        peripheral.owners.add(profile)
        peripheral.save()
        # serialize peripheral
        serializer = PeripheralSerializer(instance=peripheral)
        response_data = JSONRenderer().render(serializer.data)
        return HttpResponse(response_data, content_type="application/json")
    elif count > 0:
        response_data = {'errorCode': 3, 'errorMessage': 'multiple peripherals found with the provided major/minor'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        response_data = {'errorCode': 4, 'errorMessage': 'no peripheral found with provided major/minor'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")


@api_view(['POST'])
def disconnect_peripheral(request):
    # get code
    if not 'major' in request.DATA:
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "POST major not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    major = request.DATA['major']
    if not 'minor' in request.DATA:
        # failure
        response_data = {'errorCode': 2, 'errorMessage': "POST minor not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    minor = request.DATA['minor']
    # search a peripheral with a specific major and minor
    peripherals = Peripheral.objects.filter(major=major, minor=minor)
    count = peripherals.count()
    if count == 1:
        peripheral = peripherals.all()[0]
        # activate association
        profile = request.user.profile
        peripheral.owners.remove(profile)
        peripheral.save()
        # serialize peripheral
        serializer = PeripheralSerializer(instance=peripheral)
        response_data = JSONRenderer().render(serializer.data)
        return HttpResponse(response_data, content_type="application/json")
    elif count > 0:
        response_data = {'errorCode': 3, 'errorMessage': 'multiple peripherals found with the provided major/minor'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        response_data = {'errorCode': 4, 'errorMessage': 'no peripheral found with provided major/minor'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")


@permission_classes((IsAuthenticated,))
class PeripheralUpdateView(generics.UpdateAPIView):
    model = Peripheral
    serializer_class = PeripheralSerializer


@api_view(['POST'])
def peripheral_patch(request):
    # get id
    if not 'id' in request.DATA:
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "POST id not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    pk = request.DATA['id']
    peripheral = Peripheral.objects.get(pk=pk)
    # get name
    if not 'name' in request.DATA:
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "POST name not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    name = request.DATA['name']
    description = peripheral.description
    # get desc
    if 'desc' in request.DATA:
        description = request.DATA['desc']
    peripheral.name = name
    peripheral.description = description
    peripheral.save()
    # serialize peripheral
    serializer = PeripheralSerializer(instance=peripheral)
    response_data = JSONRenderer().render(serializer.data)
    return HttpResponse(response_data, content_type="application/json")

"""
LostFound Views
"""


@permission_classes((IsAuthenticated,))
class LostFoundListView(generics.ListAPIView):
    model = LostAndFound
    serializer_class = LostAndFoundSerializer

    def get_queryset(self):
        return LostAndFound.objects.filter(found=False)


@permission_classes((IsAuthenticated,))
class LostFoundCreateView(generics.CreateAPIView):
    model = LostAndFound
    serializer_class = LostAndFoundCreateSerializer


@permission_classes((IsAuthenticated,))
class LostFoundView(generics.RetrieveUpdateDestroyAPIView):
    model = LostAndFound
    serializer_class = LostAndFoundSerializer

"""
Security Area Views
"""

@permission_classes((IsAuthenticated,))
class SecurityAreasListView(generics.ListAPIView):
    model = SecurityArea
    serializer_class = SecurityAreaSerializer

    def get_queryset(self):
        return SecurityArea.objects.filter(user=self.request.user.profile)


@permission_classes((IsAuthenticated,))
class SecurityAreaCreateView(generics.CreateAPIView):
    model = SecurityArea
    serializer_class = SecurityAreaSerializer


@permission_classes((IsAuthenticated,))
class SecurityAreaView(generics.RetrieveUpdateDestroyAPIView):
    model = SecurityArea
    serializer_class = SecurityAreaSerializer
