from django import forms
from django.forms.widgets import HiddenInput


class AdminBulkCreateForm(forms.Form):
    lotto = forms.IntegerField(initial=1)
    from_minor = forms.IntegerField(initial=1)
    to_minor = forms.IntegerField(initial=10)
    #create_coupons = forms.BooleanField(initial=False)
    action = forms.CharField(initial='create_bulk_peripherals')

    def __init__(self, *args, **kwargs):
        super(AdminBulkCreateForm, self).__init__(*args, **kwargs)
        self.fields['action'].widget = HiddenInput()
        #self.fields['create_coupons'].widget = HiddenInput()