# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
        ('peripherals', '0005_securityarea'),
    ]

    operations = [
        migrations.AddField(
            model_name='securityarea',
            name='user',
            field=models.ForeignKey(related_name=b'security_areas', default=None, to='profiles.UserProfile'),
            preserve_default=False,
        ),
    ]
