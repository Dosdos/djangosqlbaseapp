# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('peripherals', '0006_securityarea_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='securityarea',
            name='where',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
    ]
