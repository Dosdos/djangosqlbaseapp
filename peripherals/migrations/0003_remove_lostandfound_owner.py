# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('peripherals', '0002_peripheral_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lostandfound',
            name='owner',
        ),
    ]
