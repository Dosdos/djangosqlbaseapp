# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import peripherals.models


class Migration(migrations.Migration):

    dependencies = [
        ('coupons', '__first__'),
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LostAndFound',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('enabled', models.BooleanField(default=True)),
                ('anonymous', models.BooleanField(default=False)),
                ('lost_date', models.DateTimeField(auto_now_add=True)),
                ('lost_where', models.CharField(max_length=100, null=True, blank=True)),
                ('lost_lat', models.FloatField(default=0, null=True, blank=True)),
                ('lost_lon', models.FloatField(default=0, null=True, blank=True)),
                ('lost_notes', models.CharField(max_length=200, null=True, blank=True)),
                ('found', models.BooleanField(default=False)),
                ('find_date', models.DateTimeField(auto_now_add=True)),
                ('finder', models.ForeignKey(related_name=b'lost_finder', blank=True, to='profiles.UserProfile', null=True)),
                ('owner', models.ForeignKey(related_name=b'lost_owner', blank=True, to='profiles.UserProfile', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Peripheral',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('major', models.IntegerField()),
                ('minor', models.IntegerField()),
                ('name', models.CharField(default=peripherals.models.generate_random_string, max_length=50)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('activation_code', models.CharField(default=peripherals.models.generate_random_activation_code, max_length=9)),
                ('coupon', models.ForeignKey(blank=True, to='coupons.Coupon', null=True)),
                ('owners', models.ManyToManyField(related_name=b'peripherals', null=True, to='profiles.UserProfile', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='peripheral',
            unique_together=set([('major', 'minor')]),
        ),
        migrations.AddField(
            model_name='lostandfound',
            name='peripheral',
            field=models.ForeignKey(to='peripherals.Peripheral'),
            preserve_default=True,
        ),
    ]
