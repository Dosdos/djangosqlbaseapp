# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('peripherals', '0004_lostandfound_lost_owner'),
    ]

    operations = [
        migrations.CreateModel(
            name='SecurityArea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('enabled', models.BooleanField(default=True)),
                ('lat', models.FloatField(default=0)),
                ('lon', models.FloatField(default=0)),
                ('radius', models.FloatField(default=1)),
                ('peripheral', models.ForeignKey(related_name=b'areas', to='peripherals.Peripheral')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
