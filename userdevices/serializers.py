from rest_framework import serializers

from userdevices.models import UserDevice


class UserDeviceSerializer(serializers.ModelSerializer):
    deviceuniqueid = serializers.CharField(source='device_unique_id')
    marca = serializers.CharField(source='brand')
    modello = serializers.CharField(source='model')

    class Meta:
        model = UserDevice
        fields = ['id', 'enabled', 'deviceuniqueid', 'label', 'marca', 'modello', 'push_token', 'os', 'os_version']