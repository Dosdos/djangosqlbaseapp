from django.contrib.admin import helpers
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from coupons.models import Coupon
from coupons.serializers import CouponResource


class CouponAdmin(ImportExportModelAdmin):
    resource_class = CouponResource
    list_display = ['activation_key', 'creation_date', 'activated', 'activation_date']
    ordering = ['creation_date']
    actions = ['generate_10_coupons', 'generate_50_coupons']

    # fix for empty selection
    def changelist_view(self, request, extra_context=None):
        post = request.POST.copy()
        if helpers.ACTION_CHECKBOX_NAME not in post:
            post.update({helpers.ACTION_CHECKBOX_NAME: None})
            request._set_post(post)
        return super(CouponAdmin, self).changelist_view(request, extra_context)


    def generate_coupons(self, request, queryset, num):
        print "generate_50_coupons"
        for i in range(0, num):
            coupon = Coupon(lotto="1", activation_key=Coupon.coupon_code_generate())
            coupon.save()

    def generate_10_coupons(self, request, queryset):
        self.generate_coupons(request, queryset, 10)

    generate_10_coupons.short_description = _('generate 10 new coupons')

    def generate_50_coupons(self, request, queryset):
        self.generate_coupons(request, queryset, 50)

    generate_50_coupons.short_description = _('generate 50 new coupons')


admin.site.register(Coupon, CouponAdmin)